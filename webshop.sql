-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2024. Jún 15. 11:04
-- Kiszolgáló verziója: 10.4.32-MariaDB
-- PHP verzió: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `webshop`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category_name` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- A tábla adatainak kiíratása `categories`
--

INSERT INTO `categories` (`id`, `category_name`) VALUES
(1, 'szórakoztató elektronika'),
(2, 'háztartási gép'),
(5, 'kéziszerszám');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `ordered_by_email` varchar(255) NOT NULL,
  `shipping_method` varchar(50) NOT NULL,
  `shipping_address` varchar(1000) NOT NULL,
  `total_price` int(11) NOT NULL,
  `cart` mediumtext NOT NULL,
  `order_placed_at` varchar(255) NOT NULL,
  `order_state` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- A tábla adatainak kiíratása `orders`
--

INSERT INTO `orders` (`id`, `ordered_by_email`, `shipping_method`, `shipping_address`, `total_price`, `cart`, `order_placed_at`, `order_state`) VALUES
(1, 'mikodi.erik@gmail.com', 'Házhoz szállítás', '4030, Debrecen Piac utca 11.', 899000, '[{\"product_name\":\"Számítógép\",\"quantity\":1,\"product_price\":899000,\"product_total_price\":899000}]', '2024.06.05. 18:04:55', 'Feldolgozás alatt'),
(2, 'mikodi.erik@gmail.com', 'Csomagponti átvétel', '2941 Ács, Zúgó utca 2.', 899000, '[{\"product_name\":\"Számítógép\",\"quantity\":1,\"product_price\":899000,\"product_total_price\":899000}]', '2024.06.05. 18:06:33', 'Feldolgozás alatt'),
(8, 'testelek@gmail.com', 'Házhoz szállítás', '2452, Pécs Kossuth utca 12.', 4000000, '[{\"product_name\":\"Okostelefon\",\"quantity\":10,\"product_price\":400000,\"product_total_price\":4000000}]', '2024.06.05. 18:25:19', 'Feldolgozás alatt'),
(10, 'mikodi.erik@gmail.com', 'Házhoz szállítás', '4030, Debrecen Piac utca 11.', 899000, '[{\"product_name\":\"Számítógép\",\"quantity\":1,\"product_price\":899000,\"product_total_price\":899000}]', '2024.06.05. 18:37:04', 'Kézbesítve'),
(11, 'mikodi.erik@gmail.com', 'Házhoz szállítás', '4030, Debrecen Piac utca 11.', 449000, '[{\"product_name\":\"TV\",\"quantity\":1,\"product_price\":449000,\"product_total_price\":449000}]', '2024.06.05. 18:39:01', 'Kézbesítve'),
(14, 'akovacs@gmail.com', 'Házhoz szállítás', 'Debrecen, Vásáry utca 10.', 199000, '[{\"product_name\":\"Tablet\",\"quantity\":1,\"product_price\":199000,\"product_total_price\":199000}]', '2024.06.15. 10:25:19', 'Kézbesítve'),
(15, 'akovacs@gmail.com', 'Csomagponti átvétel', '2660 Balassagyarmat, Kóvári út 6.', 1200000, '[{\"product_name\":\"Okostelefon\",\"quantity\":3,\"product_price\":400000,\"product_total_price\":1200000}]', '2024.06.15. 10:26:57', 'Feldolgozás alatt');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_name` char(255) NOT NULL,
  `product_description` text NOT NULL,
  `product_price` int(11) NOT NULL,
  `in_stock` int(11) NOT NULL,
  `category` tinytext NOT NULL,
  `image` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- A tábla adatainak kiíratása `products`
--

INSERT INTO `products` (`id`, `product_name`, `product_description`, `product_price`, `in_stock`, `category`, `image`) VALUES
(1, 'Telefon', 'Ez egy telefon', 279000, 30, 'szórakoztató elektronika', ''),
(2, 'Okostelefon', 'Ez egy okostelefon', 400000, 10, 'szórakoztató elektronika', ''),
(3, 'Tablet', 'Ez egy tablet', 199000, 27, 'szórakoztató elektronika', ''),
(4, 'Számítógép', 'Ez egy számítógép', 899000, 8, 'szórakoztató elektronika', ''),
(5, 'Okosóra', 'Ez egy okosóra', 99000, 2, 'szórakoztató elektronika', ''),
(6, 'TV', 'Ez egy televízió', 449000, 8, 'szórakoztató elektronika', ''),
(7, 'Hangszóró', 'Ez egy hangszóró', 59000, 0, 'szórakoztató elektronika', ''),
(8, 'Fejhallgató', 'Ez egy fejhallgató', 34900, 60, 'szórakoztató elektronika', ''),
(9, 'Játékkonzol', 'Ez egy játékkonzol', 349000, 15, 'szórakoztató elektronika', ''),
(10, 'Router', 'Ez egy router', 14900, 100, 'szórakoztató elektronika', ''),
(11, 'E-book olvasó', 'Ez egy e-book olvasó', 24900, 35, 'szórakoztató elektronika', ''),
(12, 'Digitális fényképezőgép', 'Ez egy digitális fényképezőgép', 179000, 25, 'szórakoztató elektronika', ''),
(13, 'Videokamera', 'Ez egy videokamera', 299000, 0, 'szórakoztató elektronika', ''),
(14, 'Power bank', 'Ez egy power bank', 9900, 75, 'szórakoztató elektronika', ''),
(15, 'Mobil tartó', 'Ez egy mobil tartó', 1990, 200, 'szórakoztató elektronika', ''),
(16, 'Bluetooth hangszóró', 'Ez egy bluetooth hangszóró', 14900, 50, 'szórakoztató elektronika', ''),
(17, 'Fülhallgató', 'Ez egy fülhallgató', 9900, 0, 'szórakoztató elektronika', ''),
(18, 'Memóriakártya', 'Ez egy memóriakártya', 5900, 150, 'szórakoztató elektronika', ''),
(19, 'Monitor', 'Ez egy monitor', 44900, 30, 'szórakoztató elektronika', ''),
(20, 'Webkamera', 'Ez egy webkamera', 9900, 100, 'szórakoztató elektronika', ''),
(25, 'Mosógép', 'Egy hatékony mosógép', 129900, 15, 'háztartási gép', ''),
(26, 'Hűtőszekrény', 'Nagy befogadóképességű hűtőszekrény', 239000, 10, 'háztartási gép', ''),
(27, 'Mosogatógép', 'Automatizált mosogatás minden napra', 189900, 8, 'háztartási gép', ''),
(28, 'Sütő', 'Modern sütő különböző funkciókkal', 149900, 12, 'háztartási gép', ''),
(29, 'Porszívó', 'Erős szívóerővel rendelkező porszívó', 89900, 20, 'háztartási gép', ''),
(30, 'Vasaló', 'Gyors és hatékony vasalás minden alkalommal', 59900, 18, 'háztartási gép', ''),
(31, 'Kávéfőző', 'Frissen őrölt kávé minden reggelre', 49900, 25, 'háztartási gép', ''),
(32, 'Mikrohullámú sütő', 'Gyors és egyszerű ételkészítés', 79900, 14, 'háztartási gép', ''),
(33, 'Légkondicionáló', 'Hatékony hűtés és fűtés otthonában', 279000, 10, 'háztartási gép', ''),
(34, 'Fúró', 'ez egy fúró', 99990, 20, 'kéziszerszám', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `role`, `address`) VALUES
(7, 'András', 'Kovács', 'akovacs@gmail.com', '$2y$10$thisismyverylongsaltse69apA6dioL9zMPMERmjN85pDSNoGJKy', 'user', 'Debrecen, Vásáry utca 10.'),

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT a táblához `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT a táblához `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
